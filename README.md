# Slides of recent talks

*  June 2020, LIG, Inria, UGA, Grenoble, France (PhD Defense) :  
    [Optimisation d’énergie en-ligne pour les systèmes temps-réels.](https://gitlab.inria.fr/splassar/slides-of-recent-talks/-/raw/master/PhDDefense_June2020.pdf?inline=true)

*  Mai 2020, EPFL, Lausanne, Suisse :  
    [Saving Energy By Tuning Processor Speed in Real-Time Systems.](https://gitlab.inria.fr/splassar/slides-of-recent-talks/-/raw/master/Mai2020.pdf?inline=true)

*  Avril 2022, Kopernic team, Inria Paris, Paris, France :  
	[link to abstract.](https://team.inria.fr/kopernic/stephan-plassart-talk/)

*  September 2022, WoNeCa2022, EPFL, Lausanne, Suisse :  
    [Equivalent versions of Total Flow Analysis.](https://gitlab.inria.fr/splassar/slides-of-recent-talks/-/raw/master/woNeCa2022.pdf?inline=true)

*  November 2022, Verimag, Grenoble, France :  
	[link to abstract.](https://www-verimag.imag.fr/Details-sur-le-seminaire.html?lang=fr&sem_id=891)
